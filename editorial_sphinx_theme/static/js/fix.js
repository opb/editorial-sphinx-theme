var sections = jQuery('#main>div>section>div').not('.prev_next_buttons_bottom');

sections.each(function (index) {
  var header = $(this).children('h1');

  $(this).prepend('<header class="main"></header>');
  $(this).append('<hr class="major">');

  var destiny = $(this).children('header.main');
  destiny.prepend(header);
});

var iconHeaderlink = '<i class="fa fa-link" aria-hidden="true"></i>';
var headerlink = jQuery('a.headerlink');
headerlink.html(iconHeaderlink);

var iconCurrentPage = '<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
var currentPage = jQuery('a.current');
currentPage.prepend(iconCurrentPage);

// Back to Top icon
jQuery(window).scroll(function () {
  if (jQuery(this).scrollTop() > 50) {
    jQuery('.scrolltop:hidden').stop(true, true).fadeIn();
  } else {
    jQuery('.scrolltop').stop(true, true).fadeOut();
  }
});

jQuery(function () {jQuery('.scroll').click(function () {
    jQuery('html,body').animate({ scrollTop: 0 }, '1000');
    return false;
  });
});
