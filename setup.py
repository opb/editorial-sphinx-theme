#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   This file is part of Editorial-Sphinx-Theme.
#
#   Editorial-Sphinx-Theme is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   at your option) any later version.
#
#   Editorial-Sphinx-Theme is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Editorial-Sphinx-Theme. If not, see <http://www.gnu.org/licenses/>.

#   Alejandro Alvarez <eliluminado00@gmail.com>

"""Setup for Editorial-Sphinx-Theme."""

import io

from setuptools import setup

from editorial_sphinx_theme import __version__

setup(
    name='editorial_sphinx_theme',
    version=__version__,
    description='A modern and responsive design Sphinx theme',
    long_description=io.open('README.rst', encoding='utf-8').read(),
    author='Alejandro Alvarez',
    author_email='eliluminado00@gmail.com',
    url='https://gitlab.com/opb/editorial-sphinx-theme',
    platforms=['any'],
    zip_safe=False,
    license='GNU General Public License v3 (GPLv3)',
    classifiers=[
        'Framework :: Sphinx',
        'Framework :: Sphinx :: Theme',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Topic :: Documentation',
        'Topic :: Software Development :: Documentation',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6'
    ],
    keywords='sphinx theme template editorial',

    packages=['editorial_sphinx_theme'],
    package_data={'editorial_sphinx_theme': [
        'theme.conf',
        '*.html',
        'static/css/*.css',
        'static/js/*.js',
        'static/font/*.*'
    ]},
    include_package_data=True,
    entry_points={
        'sphinx.html_themes': [
            'editorial_sphinx_theme = editorial_sphinx_theme',
        ]
    },
)
