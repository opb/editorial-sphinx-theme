'use strict';

/*
    Setup Steps:

    1. Install gulp globally:
    npm install -g gulp

    2. Type the following after navigating into your project folder:
    npm install

    3. Type 'gulp' and start developing


    Additional Info:

    If you suggest a better tool or corrections in configuration, open a Issue.
*/
const gulp = require('gulp');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const csslint = require('gulp-csslint');
const cleanCSS = require('gulp-clean-css');
const eslint = require('gulp-eslint');
const uglify = require('gulp-uglify');
const pump = require('pump');

gulp.task('css', function () {
  return gulp.src(['editorial_sphinx_theme/static/css/editorial.css', 'editorial_sphinx_theme/static/css/extra.css'])
    .pipe(concat('main.css'))
    .pipe(gulp.dest('editorial_sphinx_theme/static/css'));
});

// TODO: Fix all warnings by csslint
/*
gulp.task('csslint', function () {
  gulp.src(buildDir + 'static/css/editorial.css')
    .pipe(csslint())
    .pipe(csslint.formatter());
});
*/

gulp.task('minify-css', ['css'], function () {
  gulp.src('node_modules/font-awesome/css/font-awesome.min.css')
    .pipe(gulp.dest('editorial_sphinx_theme/static/css'));
  gulp.src('node_modules/font-awesome/fonts/*.*')
    .pipe(gulp.dest('editorial_sphinx_theme/static/fonts'));
  return gulp.src('editorial_sphinx_theme/static/css/main.css')
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('editorial_sphinx_theme/static/css/'));
});

gulp.task('js-lint', function () {
  return gulp.src(['editorial_sphinx_theme/static/js/main.js',
    'editorial_sphinx_theme/static/js/fix.js', '!node_modules/**'])
    .pipe(eslint({ configFile: 'package.json' }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('minify-js', [], function (cb) {
  pump([
        gulp.src(['editorial_sphinx_theme/static/js/main.js',
          'editorial_sphinx_theme/static/js/fix.js']),
        concat('main.js'),
        rename({ suffix: '.min' }),
        uglify(),
        gulp.dest('editorial_sphinx_theme/static/js'),
    ],
    cb
  );
});

gulp.task('default', ['minify-css', 'minify-js'], function () {

});
