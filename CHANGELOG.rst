*********
Changelog
*********

.. Dates must be in format YYYY-MM-DD
    Please read http://keepachangelog.com/en/0.3.0/ for more information about how to write a changelog

[1.0.0] - 2016-11-12
====================

First 'stable' version
